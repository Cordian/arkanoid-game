# Arkanoid Game
Arkanoid Game is my first project I've created to learn Java basics.  

## Table of contents
* [General game info](#info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Control](#control)
* [Features](#features)

## General game info
The game goal is to break all blocks with the moving "ball" and the paddle we control. If we won't bounce the "ball" we lose.

## Technologies
* Java 8
* Maven
* GSON 2.8.5
* Log4j 1.2.17

## Setup
To run this project, clone it using:
```
git clone https://gitlab.com/Cordian/arkanoid-game.git
```
and run in your IDE (e.g. IntelliJ IDEA).

## Control
* A - paddle left
* D - paddle right
* Up/Down arrow keys - menu navigation
* Enter - confirm
* Esc - back/pause

## Features
* **Player profiles system**  
Player can choose, delete or create profile with name no already taken. Profiles are stored in JSON files.  
<img src="https://i.postimg.cc/jj5MfYSF/profile-menu.png" width="554"/> <img src="https://i.postimg.cc/L8vyh82P/new-profile.png" width="554"/>
* **Difficulty levels**  
Player can choose one of the three difficulty levels, each of them changes "ball" speed and paddle width.  
<img src="https://i.postimg.cc/kXQY3yHf/difficulty-easy.gif" width="370"/> <img src="https://i.postimg.cc/QtKS6g5g/difficulty-medium.gif" width="370"/> <img src="https://i.postimg.cc/2ypGgqFs/difficulty-hard.gif" width="370"/>
* **Different block types**  
  * Grey block - basic block, one hit to break it
  * Orange/Yellow block - two hits to break it, after first hit becames yellow
  * Red block - one hit to break it, after hit speedups the "ball"
* **Scoreboard**  
Scoreboard displays history of games, it can be scrolled by up and down arrow keys. Scoreboard data are stored in JSON file.  
  <img src="https://i.postimg.cc/yNbvXpJY/scoreboard.png" width="555"/>
* **Saving/resuming game state**  
Player can stop the game in any moment and resume it, also after changing player's profile or rebooting app. Saves are stored in JSON files.
