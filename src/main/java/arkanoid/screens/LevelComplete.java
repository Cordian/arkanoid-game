package arkanoid.screens;

import arkanoid.Game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

// Klasa odpowiedzialna za stan gry w momencie ukonczenia poziomu
public class LevelComplete extends KeyAdapter {
    private Game game;

    public LevelComplete(Game game) {
        this.game = game;
        game.addKeyListener(this);
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isConsumed() && game.getGameState() == State.LEVEL_COMPLETE) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ENTER) {
                game.getProfile().getGameLevel().nextLevel();
                game.levelInit(game.getProfile());
                game.getProfile().getGameLevel().getPaddle().setVelocityX(0);
                game.setGameState(State.GAME);
            } else if (key == KeyEvent.VK_ESCAPE) {
                game.setGameState(State.MAIN_MENU);
            }
        }
    }

    // Rysowanie napisu "LEVEL COMPLETE"
    public void render(Graphics graphics) {
        graphics.setFont(new Font("Consolas", Font.PLAIN, 70));
        graphics.setColor(Color.ORANGE);
        graphics.drawString("LEVEL COMPLETE", Game.WIDTH / 2 - 272, 431);
        graphics.setColor(Color.RED);
        graphics.drawString("LEVEL COMPLETE", Game.WIDTH / 2 - 274, 430);
    }
}
