package arkanoid.screens;

import arkanoid.Game;
import arkanoid.profile.Profile;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

// Klasa odpowiedzialna za menu glowne
public class MainMenu extends KeyAdapter {
    private int currentChoice;
    private String[] options = {"Start", "Profile", "Scoreboard", "Quit"};
    private Game game;
    private Profile profile;

    public MainMenu(Game game) {
        currentChoice = 0;
        this.game = game;
        this.profile = game.getProfile();
        game.addKeyListener(this);
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isConsumed() && game.getGameState() == State.MAIN_MENU) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ENTER) {
                select();
            } else if (key == KeyEvent.VK_UP) {
                currentChoice--;
                if (currentChoice < 0) {
                    currentChoice = options.length - 1;
                }
            } else if (key == KeyEvent.VK_DOWN) {
                currentChoice++;
                if (currentChoice > options.length - 1) {
                    currentChoice = 0;
                }
            }
        }
    }

    // Odswiezenie aktualnego profilu uzytkownika
    public void tick() {
        profile = game.getProfile();
    }

    // Rysowanie menu glownego
    public void render(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Consolas", Font.PLAIN, 50));
        graphics.drawString("A R K A N O I D", Game.WIDTH / 2 - 200, 200);

        graphics.setFont(new Font("Consolas", Font.PLAIN, 25));
        if (profile == null || profile.isEmpty()) {
            graphics.setColor(Color.GRAY);
            graphics.drawString("Guest's profile", Game.WIDTH / 2 - 105, 225);
        } else {
            graphics.setColor(Color.ORANGE);
            graphics.drawString(
                    profile.getName() + "'s profile",
                    Game.WIDTH / 2 - (8 * (profile.getName().length() + 9) - 8),
                    228);
        }

        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        int y = 250;
        for (int i = 0; i < options.length; i++) {
            if (i == currentChoice) {
                graphics.setColor(Color.ORANGE);
            } else {
                graphics.setColor(Color.WHITE);
            }
            graphics.drawRect(Game.WIDTH / 3, y, 428, 50);
            graphics.drawString(options[i], Game.WIDTH / 2 - 8 * (options[i].length()), y + 35);
            y += 70;
        }
    }

    // Przejscie do wybranej opcji
    private void select() {
        if (currentChoice == 0) {
            game.getProfile().getGameLevel().getPaddle().setVelocityX(0);
            if (profile.getGameLevel().getDifficulty() == null) {
                game.setGameState(State.CHOOSE_DIFFICULTY);
            } else if (game.getProfile().getGameLevel().isComplete()) {
                game.setGameState(State.LEVEL_COMPLETE);
            } else {
                game.setGameState(State.GAME);
            }
        } else if (currentChoice == 1) {
            game.setGameState(State.PROFILE_MENU);
        } else if (currentChoice == 2) {
            game.setGameState(State.SCOREBOARD);
        } else if (currentChoice == options.length - 1) {
            game.stop();
        }
    }
}
