package arkanoid.screens;

import arkanoid.Game;
import arkanoid.profile.Profile;
import org.apache.log4j.Logger;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

// Klasa odpowiedzialna za wybor profilu
public class ChooseProfile extends KeyAdapter {
    private int firstRow;
    private int lastRow;
    private int currentChoice;
    private List<Profile> profileArrayList;
    private List<String> options;
    private Game game;

    public ChooseProfile(Game game, ArrayList<Profile> profileArrayList) {
        firstRow = 0;
        lastRow = 4;
        currentChoice = 0;
        this.game = game;
        this.profileArrayList = profileArrayList;
        options = new ArrayList<>();
        game.addKeyListener(this);
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isConsumed() && game.getGameState() == State.CHOOSE_PROFILE) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ENTER) {
                select();
                setDefaultView();
            } else if (key == KeyEvent.VK_ESCAPE) {
                game.setGameState(State.PROFILE_MENU);
                setDefaultView();
            } else if (key == KeyEvent.VK_UP) {
                currentChoice--;
                if (currentChoice < 0) {
                    currentChoice = 0;
                }
                if (currentChoice < firstRow) {
                    firstRow = currentChoice;
                    if (lastRow - firstRow > 4) {
                        lastRow = firstRow + 4;
                    }
                }
            } else if (key == KeyEvent.VK_DOWN) {
                currentChoice++;
                if (currentChoice > options.size() - 1) {
                    currentChoice = options.size() - 1;
                }
                if (currentChoice > lastRow) {
                    lastRow = currentChoice;
                    if (lastRow - firstRow > 4) {
                        firstRow = lastRow - 4;
                    }
                }
            }
        }
    }

    // Odswiezanie lsty aktualnych profili do wyboru
    public void tick() {
        options = new ArrayList<>();

        if (profileArrayList.isEmpty()) {
            options.add("Back");
        } else {
            for (Profile profile : profileArrayList) {
                options.add(profile.getName());
            }
        }
    }

    // Rysowanie listy aktualnych profili do wyboru
    public void render(Graphics graphics) {
        tick();
        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Consolas", Font.PLAIN, 50));
        graphics.drawString("CHOOSE PROFILE", Game.WIDTH / 2 - 190, 200);

        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        if (profileArrayList.isEmpty()) {
            graphics.setFont(new Font("Consolas", Font.PLAIN, 20));
            graphics.setColor(Color.GRAY);
            graphics.drawString("There is no profile to choose", Game.WIDTH / 3 + 55, 228);
        }

        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        int y = 250;
        for (int i = firstRow; i < options.size() && i <= lastRow; i++) {
            if (i == currentChoice) {
                graphics.setColor(Color.ORANGE);
            } else {
                graphics.setColor(Color.WHITE);
            }
            graphics.drawRect(Game.WIDTH / 3, y, 428, 50);
            graphics.drawString(options.get(i), Game.WIDTH / 2 - 8 * (options.get(i).length()), y + 35);
            y += 70;
        }
    }

    public void setProfileArrayList(ArrayList<Profile> profileArrayList) {
        this.profileArrayList = profileArrayList;
    }

    // Obsluga wybranej opcji po zatwierdzeniu klawiszem ENTER
    private void select() {
        if (profileArrayList.isEmpty()) {
            game.setGameState(State.PROFILE_MENU);
        } else if (!profileArrayList.get(currentChoice).isEmpty()) {
            game.setProfile(profileArrayList.get(currentChoice));
            Logger.getLogger(this.getClass()).info("Choosen Profile: " + game.getProfile().getName());
            game.setGameState(State.MAIN_MENU);
        } else {
            game.setGameState(State.CREATE_PROFILE);
        }
    }

    // Ustawienie domyslnych parametrow
    private void setDefaultView() {
        firstRow = 0;
        lastRow = 4;
        currentChoice = 0;
    }
}
