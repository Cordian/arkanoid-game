package arkanoid.screens;

import arkanoid.Game;
import arkanoid.game_level.Difficulty;
import org.apache.log4j.Logger;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

// Klasa odpowiedzialna za wybor poziomu trudnosci
public class ChooseDifficulty extends KeyAdapter {
    private int currentChoice;
    private String[] options = {"Easy", "Medium", "Hard"};
    private Game game;

    public ChooseDifficulty(Game game) {
        currentChoice = 0;
        this.game = game;
        game.addKeyListener(this);
    }

    // Rysowanie ekranu wyboru poziomu trudnsoci
    public void render(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Consolas", Font.PLAIN, 50));
        graphics.drawString("DIFFICULTY", Game.WIDTH / 2 - 135, 200);

        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        int y = 250;
        for (int i = 0; i < options.length; i++) {
            if (i == currentChoice) {
                if (currentChoice == 0) {
                    graphics.setColor(Color.GREEN);
                } else if (currentChoice == 1) {
                    graphics.setColor(Color.ORANGE);
                } else if (currentChoice == 2) {
                    graphics.setColor(Color.RED);
                }
            } else {
                graphics.setColor(Color.WHITE);
            }
            graphics.drawRect(Game.WIDTH / 3, y, 428, 50);
            graphics.drawString(options[i], Game.WIDTH / 2 - 8 * (options[i].length()), y + 35);
            y += 70;
        }
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isConsumed() && game.getGameState() == State.CHOOSE_DIFFICULTY) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ENTER) {
                select();
            } else if (key == KeyEvent.VK_ESCAPE) {
                currentChoice = 0;
                game.setGameState(State.MAIN_MENU);
            } else if (key == KeyEvent.VK_UP) {
                currentChoice--;
                if (currentChoice < 0) {
                    currentChoice = options.length - 1;
                }
            } else if (key == KeyEvent.VK_DOWN) {
                currentChoice++;
                if (currentChoice > options.length - 1) {
                    currentChoice = 0;
                }
            }
        }
    }

    // Obsluga wybranej opcji po zatwierdzeniu klawiszem ENTER
    private void select() {
        if (currentChoice == 0) {
            game.getProfile().getGameLevel().setDifficulty(Difficulty.EASY);
            game.setGameState(State.GAME);
        } else if (currentChoice == 1) {
            game.getProfile().getGameLevel().setDifficulty(Difficulty.MEDIUM);
            game.setGameState(State.GAME);
        } else if (currentChoice == 2) {
            game.getProfile().getGameLevel().setDifficulty(Difficulty.HARD);
            game.setGameState(State.GAME);
        } else {
            Logger.getLogger(this.getClass()).warn("ChooseDifficulty wrong choice. Choice number: " + currentChoice);
        }
        currentChoice = 0;
    }
}
