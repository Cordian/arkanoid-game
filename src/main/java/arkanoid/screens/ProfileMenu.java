package arkanoid.screens;

import arkanoid.Game;
import arkanoid.profile.Profile;
import arkanoid.profile.ProfilesManager;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

// Klasa odpowiedzialna za ekran menu profili
public class ProfileMenu extends KeyAdapter {
    private int currentChoice;
    private String[] options = {"Choose profile", "Create profile", "Delete profile", "Back"};
    private Game game;

    private ProfilesManager profilesManager;
    private ArrayList<Profile> profileArrayList;
    private ChooseProfile chooseProfile;
    private DeleteProfile deleteProfile;
    private CreateProfile createProfile;

    public ProfileMenu(Game game) {
        currentChoice = 0;
        this.game = game;
        profileArrayList = new ArrayList<>();
        initProfiles();
        chooseProfile = new ChooseProfile(game, profileArrayList);
        createProfile = new CreateProfile(game, profileArrayList);
        deleteProfile = new DeleteProfile(game, profileArrayList);
        game.addKeyListener(this);
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isConsumed() && game.getGameState() == State.PROFILE_MENU) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ENTER) {
                select();
            } else if (key == KeyEvent.VK_ESCAPE) {
                currentChoice = 0;
                game.setGameState(State.MAIN_MENU);
            } else if (key == KeyEvent.VK_UP) {
                currentChoice--;
                if (currentChoice < 0) {
                    currentChoice = options.length - 1;
                }
            } else if (key == KeyEvent.VK_DOWN) {
                currentChoice++;
                if (currentChoice > options.length - 1) {
                    currentChoice = 0;
                }
            }
        }
    }

    // Odswiezenie aktualnych profili do wyboru w opcji ChooseProfile
    public void tick() {
        chooseProfile.setProfileArrayList(profileArrayList);
    }

    // Rysowanie menu profili
    public void render(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Consolas", Font.PLAIN, 50));
        graphics.drawString("PROFILE", Game.WIDTH / 2 - 95, 200);

        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        int y = 250;
        for (int i = 0; i < options.length; i++) {
            if (i == currentChoice) {
                graphics.setColor(Color.ORANGE);
            } else {
                graphics.setColor(Color.WHITE);
            }
            graphics.drawRect(Game.WIDTH / 3, y, 428, 50);
            graphics.drawString(options[i], Game.WIDTH / 2 - 8 * (options[i].length()), y + 35);
            y += 70;
        }
    }

    public ChooseProfile getChooseProfile() {
        return chooseProfile;
    }

    public DeleteProfile getDeleteProfile() {
        return deleteProfile;
    }

    public ProfilesManager getProfilesManager() {
        return profilesManager;
    }

    public CreateProfile getCreateProfile() {
        return createProfile;
    }

    // Inicjalizacja profili z plikow JSON
    private void initProfiles() {
        profilesManager = new ProfilesManager(profileArrayList, "saves/", game);
        profilesManager.findProfiles();
        profilesManager.loadProfiles();
    }

    // Przejscie do wybranej opcji
    private void select() {
        if (currentChoice == 0) {
            game.setGameState(State.CHOOSE_PROFILE);
        } else if (currentChoice == 1) {
            game.setGameState(State.CREATE_PROFILE);
        } else if (currentChoice == 2) {
            game.setGameState(State.DELETE_PROFILE);
        } else if (currentChoice == options.length - 1) {
            currentChoice = 0;
            game.setGameState(State.MAIN_MENU);
        }
    }
}
