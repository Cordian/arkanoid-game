package arkanoid.screens;

import arkanoid.Game;
import arkanoid.game_level.GameLevel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

// Klasa odpowiedzialna za stan gry w momencie przegranej
public class GameOver extends KeyAdapter {
    private Game game;

    public GameOver(Game game) {
        this.game = game;
        game.addKeyListener(this);
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (game.getGameState() == State.GAME_OVER) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ESCAPE || key == KeyEvent.VK_ENTER) {
                game.setGameState(State.MAIN_MENU);
                game.getProfile().setGameLevel(new GameLevel());
                game.levelInit(game.getProfile());
            }
        }
    }

    // Rysowanie napisu "GAME OVER"
    public void render(Graphics graphics) {
        graphics.setFont(new Font("Consolas", Font.PLAIN, 70));
        graphics.setColor(Color.ORANGE);
        graphics.drawString("GAME OVER", Game.WIDTH / 2 - 172, 431);
        graphics.setColor(Color.RED);
        graphics.drawString("GAME OVER", Game.WIDTH / 2 - 174, 430);
    }
}
