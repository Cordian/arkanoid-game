package arkanoid.screens;

import arkanoid.Game;
import arkanoid.profile.Profile;
import org.apache.log4j.Logger;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

// Klasa odpowiedzialna za usuwanie wybranego profilu
public class DeleteProfile extends KeyAdapter {
    private int currentChoice;
    private int firstRow;
    private int lastRow;
    private List<Profile> profileArrayList;
    private List<String> options;
    private Game game;

    public DeleteProfile(Game game, ArrayList<Profile> profileArrayList) {
        currentChoice = 0;
        firstRow = 0;
        lastRow = 4;
        this.game = game;
        this.profileArrayList = profileArrayList;
        game.addKeyListener(this);

        optionsUpdate();
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isConsumed() && game.getGameState() == State.DELETE_PROFILE) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ENTER) {
                select();
            } else if (key == KeyEvent.VK_ESCAPE) {
                game.setGameState(State.PROFILE_MENU);
            } else if (key == KeyEvent.VK_UP) {
                if (currentChoice > 0) {
                    currentChoice--;
                }
            } else if (key == KeyEvent.VK_DOWN) {
                if (currentChoice < profileArrayList.size() - 1) {
                    currentChoice++;
                }
            }
        }
    }

    // Odswiezenie okna
    public void tick() {
        optionsUpdate();

        if (currentChoice < 0) {
            currentChoice = 0;
        }
        if (currentChoice < firstRow) {
            firstRow = currentChoice;
            if (lastRow - firstRow > 4) {
                lastRow = firstRow + 4;
            }
        }
        if (currentChoice > options.size() - 1) {
            currentChoice = options.size() - 1;
        }
        if (currentChoice > lastRow) {
            lastRow = currentChoice;
            if (lastRow - firstRow > 4) {
                firstRow = lastRow - 4;
            }
        }
    }

    // Rysowanie okna z dostepnymi profilami do usuniecia
    public void render(Graphics graphics) {
        graphics.setColor(Color.WHITE);

        graphics.setFont(new Font("Consolas", Font.PLAIN, 50));
        graphics.drawString("DELETE PROFILE", Game.WIDTH / 2 - 190, 200);

        if (profileArrayList.isEmpty()) {
            graphics.setFont(new Font("Consolas", Font.PLAIN, 20));
            graphics.setColor(Color.GRAY);
            graphics.drawString("There is no profile to delete", Game.WIDTH / 3 + 55, 228);
        }

        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        int y = 250;
        for (int i = firstRow; i < options.size() && i <= lastRow; i++) {
            if (options.get(0).equals("Back")) {
                graphics.setColor(Color.ORANGE);
            } else if (i == currentChoice) {
                graphics.setColor(Color.RED);
            } else {
                graphics.setColor(Color.WHITE);
            }
            graphics.drawRect(Game.WIDTH / 3, y, 428, 50);
            graphics.drawString(options.get(i), Game.WIDTH / 2 - 8 * (options.get(i).length()), y + 35);
            y += 70;
        }
    }

    // Usuniecie wskazanego profilu lub powrot do poprzedniego ekranu
    private void select() {
        if (profileArrayList.isEmpty()) {
            game.setGameState(State.PROFILE_MENU);
        } else {
            String profileName = profileArrayList.get(currentChoice).getName();
            profileArrayList.get(currentChoice).setDefaultProfile();
            game.levelInit(profileArrayList.get(currentChoice));
            profileArrayList.remove(currentChoice);
            optionsUpdate();
            Logger.getLogger(this.getClass()).info("Deleted Profile: " + profileName);
            Logger.getLogger(this.getClass()).info("Choosen Profile: Guest");
        }
    }

    // Zaaktualizowanie listy dostepnych profili do usuniecia
    private void optionsUpdate() {
        options = new ArrayList<>();

        if (profileArrayList.isEmpty()) {
            options.add("Back");
            currentChoice = 0;
        } else {
            firstRow = 0;
            lastRow = 4;
            for (Profile profile : profileArrayList) {
                options.add(profile.getName());
            }
        }
    }
}
