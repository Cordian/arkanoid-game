package arkanoid.screens;

// Typ wyliczeniowy pozwalajacy zidentyfikowac aktualny stan rozgrywki
public enum State {
    MAIN_MENU,
    GAME,
    PROFILE_MENU,
    CHOOSE_DIFFICULTY,
    CHOOSE_PROFILE,
    DELETE_PROFILE,
    CREATE_PROFILE,
    SCOREBOARD,
    GAME_OVER,
    LEVEL_COMPLETE
}
