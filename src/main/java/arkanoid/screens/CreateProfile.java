package arkanoid.screens;

import arkanoid.Game;
import arkanoid.profile.Profile;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

// Klasa odpowiedzialna za utworzenie nowego profilu
public class CreateProfile extends KeyAdapter {
    private String profileName;
    private Game game;
    private List<Profile> profileArrayList;
    private List<String> profileNamesList;

    public CreateProfile(Game game, List<Profile> profileArrayList) {
        profileName = "";
        this.profileArrayList = profileArrayList;
        this.game = game;
        game.addKeyListener(this);
        profileNamesUpdate();
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isConsumed() && game.getGameState() == State.CREATE_PROFILE) {
            int key = keyEvent.getKeyChar();
            keyEvent.consume();

            if (key == KeyEvent.VK_ENTER) {
                if (!profileName.isEmpty() && !doesProfileExist()) {
                    Profile profile = new Profile();
                    profile.newProfile(profileName);
                    game.levelInit(profile);
                    profileArrayList.add(profile);

                    profileName = "";
                    game.getProfileMenu().getChooseProfile().tick();
                    game.getProfileMenu().getDeleteProfile().tick();
                    game.setGameState(State.PROFILE_MENU);
                }
            } else if (key == KeyEvent.VK_ESCAPE) {
                profileName = "";
                game.setGameState(State.PROFILE_MENU);
            } else if ((key >= 48 && key <= 57) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122)) {
                if (profileName.length() < 10) {
                    profileName += (char) key;
                }
            } else if (key == KeyEvent.VK_BACK_SPACE) {
                if (!profileName.isEmpty()) {
                    profileName = profileName.substring(0, profileName.length() - 1);
                }
            }
        }
    }

    // Odswiezenie nazw istniejacych profili
    public void tick() {
        profileNamesUpdate();
    }

    // Rysowanie okna tworzenia nowego profilu
    public void render(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        int y = 250;

        graphics.setFont(new Font("Consolas", Font.PLAIN, 50));
        graphics.drawString("NEW PROFILE", Game.WIDTH / 2 - 145, 200);

        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        graphics.drawRect(Game.WIDTH / 3, y, 428, 50);
        graphics.drawString(profileName, Game.WIDTH / 2 - (8 * (profileName.length())), y + 35);

        if (doesProfileExist()) {
            graphics.setColor(Color.RED);
            graphics.setFont(new Font("Consolas", Font.PLAIN, 20));
            graphics.drawString("Profile with that name already exists!", Game.WIDTH / 3 + 5, 228);

        } else if (profileName.isEmpty()) {
            graphics.setColor(Color.GRAY);
            graphics.setFont(new Font("Consolas", Font.PLAIN, 20));
            graphics.drawString("Profile name is empty", Game.WIDTH / 3 + 100, 228);
        } else {
            graphics.setColor(Color.ORANGE);
        }
        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        graphics.drawRect(Game.WIDTH / 3, y + 70, 428, 50);
        graphics.drawString("Create", Game.WIDTH / 2 - 45, y + 35 + 70);
    }

    // Zaaktualizowanie nazw instniejacych profili
    private void profileNamesUpdate() {
        profileNamesList = new ArrayList<>();
        if (!profileArrayList.isEmpty()) {
            for (Profile profile : profileArrayList) {
                profileNamesList.add(profile.getName());
            }
        }
    }

    private boolean doesProfileExist() {
        for (String currentProfileName : profileNamesList) {
            if (currentProfileName.equals(profileName)) {
                return true;
            }
        }
        return false;
    }
}
