package arkanoid.game_level.game_objects;

// Typ wyliczeniowy pozwalajacy zidentyfikowac GameObject
public enum GameObjectId {
    PADDLE,
    BALL,
    BASIC_BLOCK,
    DOUBLE_BLOCK,
    SPEEDUP_BLOCK
}
