package arkanoid.game_level.game_objects;

import arkanoid.Game;

import java.awt.Color;
import java.awt.Graphics;

import static arkanoid.Game.HEIGHT;
import static arkanoid.Game.WIDTH;

public class Paddle extends GameObject {
    private int defaultSpeed;
    private int currentSpeed;
    private Color color;

    public Paddle(int width, int height) {
        super((WIDTH / 2) - width / 2, 85 * HEIGHT / 100, GameObjectId.PADDLE);
        this.width = width;
        this.height = height;
        defaultSpeed = 7;
        currentSpeed = 7;
        color = Color.WHITE;
    }

    // Zmiana polozenia
    public void tick() {
        for (int i = 0; i < currentSpeed; i++) {
            x += velocityX;
            y += velocityY;
        }
    }

    // Zaaktualizowanie polozenia oraz sprawdzenie ograniczen
    public void update() {
        tick();
        checkRestriction();
    }

    // Rysowanie platformy
    public void render(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(x, y, width, height);
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public int getDefaultSpeed() {
        return defaultSpeed;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    // Sprawdzenie ograniczen zwiazanych ze scianami
    private void checkRestriction() {
        if (x <= 0) {
            x = 0;
        }
        if (x + width >= Game.WIDTH - 8) {
            x = Game.WIDTH - 8 - width;
        }
    }
}
