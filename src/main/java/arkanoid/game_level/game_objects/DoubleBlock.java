package arkanoid.game_level.game_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.util.concurrent.atomic.AtomicInteger;

public class DoubleBlock extends GameObject {
    private Color color;
    private AtomicInteger collisionCounter;

    public DoubleBlock(int x, int y, int width, int height) {
        super(x, y, GameObjectId.DOUBLE_BLOCK);
        collisionCounter = new AtomicInteger(1);
        this.color = Color.ORANGE;

        this.width = width;
        this.height = height;

        velocityX = 0;
        velocityY = 0;
    }

    // Rysowanie bloku DoubleBlock
    public void render(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(x, y, width, height);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public AtomicInteger getCollisionCounter() {
        return collisionCounter;
    }
}
