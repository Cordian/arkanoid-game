package arkanoid.game_level.game_objects;

import java.awt.Color;
import java.awt.Graphics;

public class BasicBlock extends GameObject {
    private Color color;

    public BasicBlock(int x, int y, int width, int height) {
        super(x, y, GameObjectId.BASIC_BLOCK);
        this.color = Color.GRAY;

        this.width = width;
        this.height = height;

        velocityX = 0;
        velocityY = 0;
    }

    // Rysowanie bloku BasicBlock
    public void render(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(x, y, width, height);
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
