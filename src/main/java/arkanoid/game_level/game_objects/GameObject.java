package arkanoid.game_level.game_objects;

// Klasa z ktorej dziedzicza klasy:
// Ball, Paddle, BasicBlock, DoubleBlock, SpeedupBlock
public abstract class GameObject {
    protected int x;
    protected int y;
    protected int width;
    protected int height;
    protected int velocityX;
    protected int velocityY;
    protected GameObjectId gameObjectId;

    public GameObject(int x, int y, GameObjectId gameObjectId) {
        this.x = x;
        this.y = y;
        this.gameObjectId = gameObjectId;
        velocityX = 0;
        velocityY = 0;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public GameObjectId getGameObjectId() {
        return gameObjectId;
    }

    public int getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(int velocityX) {
        this.velocityX = velocityX;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }
}
