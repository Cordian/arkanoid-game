package arkanoid.game_level.game_objects;

import java.awt.Color;
import java.awt.Graphics;

public class SpeedupBlock extends GameObject {
    private Color color;

    public SpeedupBlock(int x, int y, int width, int height) {
        super(x, y, GameObjectId.SPEEDUP_BLOCK);
        this.color = Color.RED;

        this.width = width;
        this.height = height;

        velocityX = 0;
        velocityY = 0;
    }

    // Rysowanie bloku SpeedupBlock
    public void render(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(x, y, width, height);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
