package arkanoid.game_level.game_objects;

import arkanoid.Game;
import arkanoid.game_level.ScoreDisplay;
import arkanoid.game_level.handlers.BasicBlockHandler;
import arkanoid.game_level.handlers.DoubleBlockHandler;
import arkanoid.game_level.handlers.SpeedupBlockHandler;
import org.apache.log4j.Logger;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Ball extends GameObject {
    private int defaultSpeed;
    private int currentSpeed;
    private Color color;

    public Ball(int x, int y) {
        super(x, y, GameObjectId.BALL);
        color = randomColor();
        width = 32;
        height = 32;
        defaultSpeed = 3;
        currentSpeed = defaultSpeed;
        velocityX = -1;
        velocityY = -1;
    }

    // Aktualizowanie pozycji Pilki oraz sprawdzenie kolizji z innymi obiektami
    public boolean update(Paddle paddle,
                          BasicBlockHandler basicBlockHandler,
                          DoubleBlockHandler doubleBlockHandler,
                          SpeedupBlockHandler speedupBlockHandler,
                          ScoreDisplay scoreDisplay) {
        for (int i = 0; i < currentSpeed; i++) {
            x += velocityX;
            y += velocityY;

            if (checkRestriction()) {
                return true;
            }
            checkPaddleCollision(paddle);
            checkBasicBlockCollision(basicBlockHandler, scoreDisplay);
            checkDoubleBlockCollision(doubleBlockHandler, scoreDisplay);
            checkSpeedupBlockCollision(speedupBlockHandler, scoreDisplay);
        }
        return false;
    }

    // Rysowanie Pilki
    public void render(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(x, y, width, height);
    }

    public Color getColor() {
        return color;
    }

    public void setDefaultSpeed(int speed) {
        currentSpeed = defaultSpeed = speed;
    }

    private Color randomColor() {
        Random random = new Random();
        float r = random.nextFloat();
        if (r < 0.3f) {
            r += 0.3f;
        }
        float g = random.nextFloat();
        if (g < 0.3f) {
            g += 0.3f;
        }
        float b = random.nextFloat();
        if (b < 0.3f) {
            b += 0.3f;
        }

        return new Color(r, g, b);
    }

    // Sprawdzenie kolizji ze scianami
    private boolean checkRestriction() {
        if (x <= 1) {
            velocityX = 1;
            Logger.getLogger(this.getClass()).info("Left wall collision.");
        }
        if ((x + width) >= (Game.WIDTH - 8)) {
            velocityX = -1;
            Logger.getLogger(this.getClass()).info("Right wall collision.");
        }
        if (y <= 0) {
            velocityY = 1;
            Logger.getLogger(this.getClass()).info("Upper wall collision.");
        }

        return y + height >= Game.HEIGHT - 32;
    }

    // Sprawdzenie kolizji Pilki z Platforma
    private void checkPaddleCollision(Paddle paddle) {
        if (paddle != null) {
            // Jesli wspolrzedna X Pilki znajduje sie w obszarze
            // wspolrzednej X Platformy
            if (x + width >= paddle.getX() && x <= paddle.getX() + paddle.getWidth()) {
                // Jesli dolna krawedz Pilki (Y) == gorna krawedz Platformy (Y)
                if (y + height == paddle.getY()) {
                    velocityY = -1;
                    currentSpeed = defaultSpeed;
                    Logger.getLogger(this.getClass()).info("Top side Paddle collision.");
                }
                // Jesli dolna krawedz Pilki (Y) > gorna krawedz Platformy (Y), oraz
                // gorna krawedz Pilki (Y) < dolna krawedz Platformy (Y)
                else if (y + height > paddle.getY() && y < paddle.getY() + paddle.getHeight()) {
                    // Jesli Pilka porusza sie w lewo i Platforma porusza sie w lewo
                    if (velocityX < 0 && paddle.getVelocityX() < 0) {
                        currentSpeed = paddle.getDefaultSpeed();
                        paddle.setX(paddle.getX() + 1);
                        paddle.setCurrentSpeed(defaultSpeed);
                        Logger.getLogger(this.getClass()).info("Left side Paddle collision (moving in the same direction).");
                    }
                    // Jesli Pilka porusza sie w lewo i Platforma porusza sie w prawo
                    else if (velocityX < 0 && paddle.getVelocityX() > 0) {
                        velocityX = 1;
                        currentSpeed = paddle.getDefaultSpeed();
                        Logger.getLogger(this.getClass()).info("Right side Paddle collision (moving in opposite direction).");
                    }
                    // Jesli Pilka porusza sie w prawo i Platforma porusza sie w prawo
                    else if (velocityX > 0 && paddle.getVelocityX() > 0) {
                        currentSpeed = paddle.getDefaultSpeed();
                        paddle.setX(paddle.getX() - 1);
                        paddle.setCurrentSpeed(defaultSpeed);
                        Logger.getLogger(this.getClass()).info("Right side Paddle collision (moving in the same direction).");
                    }
                    // Jesli Pilka porusza sie w prawo i Platforma porusza sie w lewo
                    else if (velocityX > 0 && paddle.getVelocityX() < 0) {
                        velocityX = -1;
                        currentSpeed = paddle.getDefaultSpeed();
                        Logger.getLogger(this.getClass()).info("Left side Paddle collision (moving in opposite direction).");
                    }
                    // W przeciwnym wypadku
                    // (jesli Pilka porusza sie w lewo/prawo i Platforma sie nie porusza)
                    else {
                        velocityX *= -1;
                        Logger.getLogger(this.getClass()).info("Left/right side Paddle (not moving) collision.");
                    }
                }
            }
            // Jesli Pilka nie znajduje sie w obszarze Platformy, a wiec na pewno nie doszlo do kolizji
            // wtedy Platforma porusza sie z domyslna szybkoscia
            else {
                paddle.setCurrentSpeed(paddle.getDefaultSpeed());
            }
        }
    }

    // Sprawdzenie kolizji Pilki z blokiem BasicBlock
    private void checkBasicBlockCollision(BasicBlockHandler basicBlockHandler, ScoreDisplay scoreDisplay) {
        BasicBlock basicBlock;
        if (basicBlockHandler != null) {
            // Sprawdzenie kazdego bloku BasicBlock
            for (int i = 0; i < basicBlockHandler.getBasicBlocks().size(); i++) {
                basicBlock = basicBlockHandler.getBasicBlocks().get(i);
                // Jesli nastapila kolizja
                if (isCollision(basicBlock)) {
                    Logger.getLogger(this.getClass()).info("BasicBlock collision.");
                    // Jesli pomyslnie usunieto blok
                    if (basicBlockHandler.removeGameObject(basicBlock)) {
                        scoreDisplay.incrementAndGet();
                    }
                }
            }
        }
    }

    // Sprawdzenie kolizji Pilki z blokiem DoubleBlock
    private void checkDoubleBlockCollision(DoubleBlockHandler doubleBlockHandler, ScoreDisplay scoreDisplay) {
        DoubleBlock doubleBlock;
        if (doubleBlockHandler != null) {
            // Sprawdzenie kazdego bloku DoubleBlock
            for (int i = 0; i < doubleBlockHandler.getDoubleBlocks().size(); i++) {
                doubleBlock = doubleBlockHandler.getDoubleBlocks().get(i);
                // Jesli nastapila kolizja
                if (isCollision(doubleBlock)) {
                    Logger.getLogger(this.getClass()).info("DoubleBlock collision.");
                    // Jesli pomyslnie usunieto blok
                    if (doubleBlockHandler.removeGameObject(doubleBlock)) {
                        scoreDisplay.incrementAndGet();
                    }
                }
            }
        }
    }

    // Sprawdzenie kolizji Pilki z blokiem DoubleBlock
    private void checkSpeedupBlockCollision(SpeedupBlockHandler speedupBlockHandler, ScoreDisplay scoreDisplay) {
        SpeedupBlock speedupBlock;
        if (speedupBlockHandler != null) {
            for (int i = 0; i < speedupBlockHandler.getSpeedupBlocks().size(); i++) {
                speedupBlock = speedupBlockHandler.getSpeedupBlocks().get(i);
                // Jesli nastapila kolizja
                if (isCollision(speedupBlock)) {
                    Logger.getLogger(this.getClass()).info("SpeedupBlock collision.");
                    // Jesli pomyslnie usunieto blok
                    if (speedupBlockHandler.removeGameObject(speedupBlock)) {
                        scoreDisplay.incrementAndGet();
                        currentSpeed++;
                    }
                }
            }
        }
    }

    // Metoda sprawdza czy wystapila kolizcja Pilki z dowolnym blokiem
    private boolean isCollision(GameObject gameObject) {
        if (gameObject.getGameObjectId() == GameObjectId.BASIC_BLOCK ||
                gameObject.getGameObjectId() == GameObjectId.DOUBLE_BLOCK ||
                gameObject.getGameObjectId() == GameObjectId.SPEEDUP_BLOCK) {

            // Jesli wspolrzedna X Pilki znajduje sie w obszarze wspolrzednej X bloku
            // (kolizja z dolna i gorna krawedzia bloku)
            if (x <= gameObject.getX() + gameObject.getWidth() && x + width >= gameObject.getX()) {
                // Jesli gorna krawedz Pilki == dolna krawedz bloku
                if (y == gameObject.getY() + gameObject.getHeight()) {
                    velocityY = 1;
                    return true;
                }
                // Jesli dolna krawedz Pilki == gorna krawedz bloku
                else if (y + height == gameObject.getY()) {
                    velocityY = -1;
                    return true;
                }
            }

            // Jesli wspolrzedna Y Pilki znajduje sie w obszarze wspolrzednej Y bloku
            // (kolizja z lewa i prawa krawedzia bloku)
            if (y <= gameObject.getY() + gameObject.getHeight() && y + height >= gameObject.getY()) {
                // Jesli lewa krawedz Pilki == prawa krawedz bloku
                if (x == gameObject.getX() + gameObject.getWidth()) {
                    velocityX = 1;
                    return true;
                }
                // Jesli prawa krawedz Pilki == lewa krawedz bloku
                else if (x + width == gameObject.getX()) {
                    velocityX = -1;
                    return true;
                }
            }
        }
        // Nie nastapila kolizja
        return false;
    }
}
