package arkanoid.game_level;

import arkanoid.game_level.game_objects.Ball;
import arkanoid.game_level.game_objects.Paddle;
import arkanoid.game_level.handlers.BasicBlockHandler;
import arkanoid.game_level.handlers.DoubleBlockHandler;
import arkanoid.game_level.handlers.SpeedupBlockHandler;
import org.apache.log4j.Logger;

import java.awt.Graphics;

import static arkanoid.Game.HEIGHT;
import static arkanoid.Game.WIDTH;

// Klasa odpowiedzialna za aktualnie rozgrywany poziom
public class GameLevel {
    private Paddle paddle;
    private Ball ball;
    private BasicBlockHandler basicBlockHandler;
    private DoubleBlockHandler doubleBlockHandler;
    private SpeedupBlockHandler speedupBlockHandler;
    private Difficulty difficulty;
    private ScoreDisplay scoreDisplay;

    public GameLevel() {
        paddle = new Paddle(128, 8);
        ball = new Ball(WIDTH / 2, 70 * HEIGHT / 100);
        basicBlockHandler = new BasicBlockHandler();
        doubleBlockHandler = new DoubleBlockHandler();
        speedupBlockHandler = new SpeedupBlockHandler();
        scoreDisplay = new ScoreDisplay();
        difficulty = null;
    }

    // Zaaktualizowanie elementow rozgrywki
    public boolean tick() {
        paddle.update();
        return ball.update(paddle, basicBlockHandler, doubleBlockHandler, speedupBlockHandler, scoreDisplay);
    }

    // Rysowanie elementow rozgrywki
    public void render(Graphics graphics) {
        ball.render(graphics);
        paddle.render(graphics);
        basicBlockHandler.render(graphics);
        doubleBlockHandler.render(graphics);
        speedupBlockHandler.render(graphics);
        scoreDisplay.render(graphics);
    }

    // Sprawdzenie czy aktualna rozgrywka zostala ukonczona
    public boolean isComplete() {
        return (basicBlockHandler.getBasicBlocks().isEmpty() &&
                doubleBlockHandler.getDoubleBlocks().isEmpty() &&
                speedupBlockHandler.getSpeedupBlocks().isEmpty());
    }

    public BasicBlockHandler getBasicBlockHandler() {
        return basicBlockHandler;
    }

    public DoubleBlockHandler getDoubleBlockHandler() {
        return doubleBlockHandler;
    }

    public SpeedupBlockHandler getSpeedupBlockHandler() {
        return speedupBlockHandler;
    }

    public ScoreDisplay getScoreDisplay() {
        return scoreDisplay;
    }

    public Paddle getPaddle() {
        return paddle;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    // Ustawienie wybranego poziomu trudnosci
    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
        setLevelByDifficulty();
        Logger.getLogger(this.getClass()).info("Choosen difficulty: " + difficulty);
    }

    // Reset polozenia pilki oraz platformy
    public void nextLevel() {
        ball = new Ball(WIDTH / 2, 70 * HEIGHT / 100);
        setLevelByDifficulty();
    }

    // Ustawienie szerokosci platformy oraz szybkosci pilki
    // na podstawie poziomu trudnosci
    private void setLevelByDifficulty() {
        if (difficulty.equals(Difficulty.EASY)) {
            paddle.setWidth(128);
            paddle.setX((WIDTH / 2) - 64);
            ball.setDefaultSpeed(3);
        } else if (difficulty.equals(Difficulty.MEDIUM)) {
            paddle.setWidth(64);
            paddle.setX((WIDTH / 2) - 32);
            ball.setDefaultSpeed(4);
        } else if (difficulty.equals(Difficulty.HARD)) {
            paddle.setWidth(32);
            paddle.setX((WIDTH / 2) - 16);
            ball.setDefaultSpeed(5);
        }
    }
}
