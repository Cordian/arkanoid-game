package arkanoid.game_level;

import java.util.ArrayList;
import java.util.List;

// Typ wyliczeniowy okreslajacy poziom trudnosci
public enum Difficulty {
    EASY("Easy", 0),
    MEDIUM("Medium", 1),
    HARD("Hard", 2);

    public String name;
    public int id;
    public static List<String> list = new ArrayList<>();

    private Difficulty(String name, int id) {
        this.name = name;
        this.id = id;
    }

    static {
        for (Difficulty difficulty: Difficulty.values()) {
            list.add(difficulty.name);
        }
    }

   /* public String[] getTypes() {
        List<String> list = new ArrayList<>();
        for(Difficulty difficulty: Difficulty.values()) {
            list.add(difficulty.name);
        }

        return (String[]) list.toArray();
    }*/
}


