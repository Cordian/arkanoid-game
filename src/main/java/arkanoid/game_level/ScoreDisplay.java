package arkanoid.game_level;

import arkanoid.Game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.concurrent.atomic.AtomicInteger;

// Klasa odpowiedzialna za wyswietlanie aktualnego wyniku
public class ScoreDisplay {
    private AtomicInteger score;

    public ScoreDisplay() {
        score = new AtomicInteger(0);
    }

    // Rysowanie aktualnego wyniku
    public void render(Graphics graphics) {
        graphics.setColor(Color.GRAY);
        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        graphics.drawString("SCORE: " + score.get(), 5, Game.HEIGHT - 40);
    }

    public int getScore() {
        return score.get();
    }

    public void incrementAndGet() {
        for (int i = 0; i < 10; i++) {
            score.incrementAndGet();
        }
    }
}
