package arkanoid.game_level.handlers;

import arkanoid.game_level.game_objects.BasicBlock;

import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;


public class BasicBlockHandler {
    private List<BasicBlock> basicBlocks;

    public BasicBlockHandler() {
        basicBlocks = new LinkedList<>();
    }

    public List<BasicBlock> getBasicBlocks() {
        return basicBlocks;
    }

    public void render(Graphics graphics) {
        for (BasicBlock basicBlock : basicBlocks) {
            basicBlock.render(graphics);
        }
    }

    public void addGameObject(BasicBlock basicBlock) {
        basicBlocks.add(basicBlock);
    }

    public boolean removeGameObject(BasicBlock basicBlock) {
        return basicBlocks.remove(basicBlock);
    }

}
