package arkanoid.game_level.handlers;

import arkanoid.game_level.game_objects.GameObjectId;
import arkanoid.game_level.game_objects.DoubleBlock;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;


public class DoubleBlockHandler {
    private List<DoubleBlock> doubleBlocks;

    public DoubleBlockHandler() {
        doubleBlocks = new LinkedList<>();
    }

    public List<DoubleBlock> getDoubleBlocks() {
        return doubleBlocks;
    }

    public void render(Graphics graphics) {
        for (DoubleBlock gameObject : doubleBlocks) {
            gameObject.render(graphics);
        }
    }

    public void addGameObject(DoubleBlock gameObject) {
        doubleBlocks.add(gameObject);
    }

    public boolean removeGameObject(DoubleBlock doubleBlock) {
        if (doubleBlock.getGameObjectId().equals(GameObjectId.DOUBLE_BLOCK)) {
            if (( doubleBlock).getCollisionCounter().get() != 0) {
                ( doubleBlock).getCollisionCounter().decrementAndGet();
                (doubleBlock).setColor(Color.YELLOW);
                return false;
            }
        }
        return doubleBlocks.remove(doubleBlock);
    }

}
