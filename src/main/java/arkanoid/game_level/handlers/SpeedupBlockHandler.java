package arkanoid.game_level.handlers;

import arkanoid.game_level.game_objects.SpeedupBlock;

import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;


public class SpeedupBlockHandler {
    private List<SpeedupBlock> speedupBlocks;

    public SpeedupBlockHandler() {
        speedupBlocks = new LinkedList<>();
    }

    public List<SpeedupBlock> getSpeedupBlocks() {
        return speedupBlocks;
    }

    public void render(Graphics graphics) {
        for (SpeedupBlock speedupBlock : speedupBlocks) {
            speedupBlock.render(graphics);
        }
    }

    public void addGameObject(SpeedupBlock speedupBlock) {
        speedupBlocks.add(speedupBlock);
    }

    public boolean removeGameObject(SpeedupBlock speedupBlock) {
        return speedupBlocks.remove(speedupBlock);
    }

}
