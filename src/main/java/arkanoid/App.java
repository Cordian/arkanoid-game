package arkanoid;

import org.apache.log4j.Logger;

public class App {
    public static void main(String[] args) {
        Logger.getLogger(App.class).info("Starting Arkanoid game.");
        new Game();
    }
}
