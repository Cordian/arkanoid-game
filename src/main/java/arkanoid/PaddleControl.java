package arkanoid;

import arkanoid.game_level.game_objects.Paddle;
import arkanoid.screens.State;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

// Klasa odpowiedzialna za sterowanie platforma
public class PaddleControl extends KeyAdapter {
    private Game game;
    private Paddle paddle;

    public PaddleControl(Game game, Paddle paddle) {
        this.game = game;
        this.paddle = paddle;
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (game.getGameState() == State.GAME) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ESCAPE) {
                game.setGameState(State.MAIN_MENU);
            }
            if (key == KeyEvent.VK_A) {
                paddle.setVelocityX(-1);
            }
            if (key == KeyEvent.VK_D) {
                paddle.setVelocityX(1);
            }
        }
    }

    // Obsluga puszczonego klawisza
    public void keyReleased(KeyEvent keyEvent) {
        if (game.getGameState() == State.GAME) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_A) {
                if (paddle.getVelocityX() < 0) {
                    paddle.setVelocityX(0);
                }
            }
            if (key == KeyEvent.VK_D) {
                if (paddle.getVelocityX() > 0) {
                    paddle.setVelocityX(0);
                }
            }
        }
    }
}
