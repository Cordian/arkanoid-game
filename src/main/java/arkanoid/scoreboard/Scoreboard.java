package arkanoid.scoreboard;

import arkanoid.Game;
import arkanoid.game_level.Difficulty;
import arkanoid.screens.State;
import org.apache.log4j.Logger;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

// Klasa odpowiedzialna za wyswietlanie tablicy wynikow
public class Scoreboard extends KeyAdapter {
    private int firstRow;
    private int lastRow;
    private int defaultFirstRow;
    private int defaultLastRow;
    private List<ScoreboardRow> scoreboardRows;
    private ScoreboardManager scoreboardManager;
    private Game game;

    public Scoreboard(Game game) {
        this.game = game;
        game.addKeyListener(this);
        scoreboardRows = new ArrayList<>();
        scoreboardManager = new ScoreboardManager("scoreboard.json", scoreboardRows);
        scoreboardManager.loadScoreboard();
        leaderboardUpdate();
    }

    // Ustawienie zakresu wyswietlanych wynikow na podstawie listy wynikow
    public void leaderboardUpdate() {
        firstRow = 0;
        defaultFirstRow = 0;
        if (scoreboardRows.size() > 5) {
            lastRow = 5;
            defaultLastRow = 5;
        } else {
            lastRow = scoreboardRows.size();
            defaultLastRow = scoreboardRows.size();
        }
    }

    // Obsluga wcisnietego klawisza
    public void keyPressed(KeyEvent keyEvent) {
        if (!keyEvent.isConsumed() && game.getGameState() == State.SCOREBOARD) {
            int key = keyEvent.getKeyCode();
            keyEvent.consume();

            if (key == KeyEvent.VK_ENTER || key == KeyEvent.VK_ESCAPE) {
                firstRow = defaultFirstRow;
                lastRow = defaultLastRow;
                game.setGameState(State.MAIN_MENU);
            } else if (key == KeyEvent.VK_UP) {
                if (scoreboardRows.size() > 5) {
                    firstRow--;
                    if (firstRow < 0) {
                        firstRow = 0;
                    } else {
                        lastRow--;
                    }
                }
            } else if (key == KeyEvent.VK_DOWN) {
                if (scoreboardRows.size() > 5) {
                    lastRow++;
                    if (lastRow > scoreboardRows.size()) {
                        lastRow = scoreboardRows.size();
                    } else {
                        firstRow++;
                    }
                }
            } else if (key == KeyEvent.VK_X) {
                scoreboardRows.clear();
            }
        }
    }

    // Rysowanie tablicy wynikow
    public void render(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Consolas", Font.PLAIN, 50));
        graphics.drawString("SCOREBOARD", Game.WIDTH / 2 - 140, 100);

        if (scoreboardRows.isEmpty()) {
            graphics.setFont(new Font("Consolas", Font.PLAIN, 20));
            graphics.setColor(Color.GRAY);
            graphics.drawString("Scoreboard is empty", Game.WIDTH / 3 + 105, 128);
        } else {
            graphics.setFont(new Font("Consolas", Font.PLAIN, 20));
            graphics.setColor(Color.GRAY);
            graphics.drawString("Score", Game.WIDTH / 5 + 16, 138);
            graphics.drawString("Difficulty", Game.WIDTH / 5 + 110, 138);
            graphics.drawString("Profile", Game.WIDTH / 5 + 270, 138);
            graphics.drawString("Date", Game.WIDTH / 5 + 480, 138);
        }

        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        int y = 150;
        for (int i = firstRow; i < lastRow && i < scoreboardRows.size(); i++) {
            graphics.setColor(Color.WHITE);
            graphics.drawRect(Game.WIDTH / 5, y, 768, 50);
            graphics.drawString(Integer.toString(scoreboardRows.get(i).getScore()), Game.WIDTH / 5 + 16, y + 35);
            Difficulty difficulty = scoreboardRows.get(i).getDifficulty();
            String difficultyText = "";

            if (difficulty.equals(Difficulty.EASY)) {
                graphics.setColor(Color.GREEN);
                difficultyText = "EASY";
            } else if (difficulty.equals(Difficulty.MEDIUM)) {
                graphics.setColor(Color.ORANGE);
                difficultyText = "MEDIUM";
            } else if (difficulty.equals(Difficulty.HARD)) {
                graphics.setColor(Color.RED);
                difficultyText = "HARD";
            }
            graphics.drawString(difficultyText, Game.WIDTH / 5 + 110, y + 35);
            graphics.setColor(Color.WHITE);
            graphics.drawString(scoreboardRows.get(i).getProfileName(), Game.WIDTH / 5 + 270, y + 35);
            graphics.drawString(scoreboardRows.get(i).getDate(), Game.WIDTH / 5 + 480, y + 35);
            y += 70;
        }

        if (!scoreboardRows.isEmpty()) {
            graphics.setFont(new Font("Consolas", Font.PLAIN, 20));
            graphics.setColor(Color.RED);
            graphics.drawRect(Game.WIDTH / 3 + 129, 171 + 5 * 75, 20, 24);
            graphics.drawString("Press X to clear Scoreboard", Game.WIDTH / 3 + 68, 160 + 5 * 70 + 55);
        }

        graphics.setFont(new Font("Consolas", Font.PLAIN, 30));
        graphics.setColor(Color.ORANGE);
        graphics.drawRect(Game.WIDTH / 3, 160 + 6 * 70, 428, 50);
        graphics.drawString("Back", Game.WIDTH / 2 - (8 * 4), 160 + 6 * 70 + 35);
    }

    // Dodanie nowego wyniku do listy
    public void newScoreboardRow(int score, String profileName, Difficulty difficulty) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        ScoreboardRow scoreboardRow = new ScoreboardRow(
                score,
                profileName,
                dateTimeFormatter.format(LocalDateTime.now()),
                difficulty);
        scoreboardRows.add(0, scoreboardRow);
        leaderboardUpdate();
        Logger.getLogger(this.getClass()).info("Scoreboard updated.");
    }

    public ScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }
}
