package arkanoid.scoreboard;

import arkanoid.game_level.Difficulty;

// Klasa realizująca pojedynczy wiersz tablicy wynikow
public class ScoreboardRow {
    private int score;
    private String profileName;
    private String date;
    private Difficulty difficulty;

    public ScoreboardRow(int score, String profileName, String date, Difficulty difficulty) {
        this.score = score;
        this.profileName = profileName;
        this.date = date;
        this.difficulty = difficulty;
    }

    public int getScore() {
        return score;
    }

    public String getProfileName() {
        return profileName;
    }

    public String getDate() {
        return date;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }
}
