package arkanoid.scoreboard;

import arkanoid.converters.ScoreboardJsonConverter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

// Klasa odpowiedzialna za zarzadzanie lista wynikow
public class ScoreboardManager {
    private File scoreboardFile;
    private List<ScoreboardRow> scoreboardRows;

    public ScoreboardManager(String path, List<ScoreboardRow> scoreboardRows) {
        scoreboardFile = new File(path);
        this.scoreboardRows = scoreboardRows;
    }

    // Zaladowanie profili z pliku JSON
    public void loadScoreboard() {
        try {
            ScoreboardJsonConverter scoreboardJsonConverter = new ScoreboardJsonConverter(scoreboardFile.getPath());
            ArrayList<ScoreboardRow> tempScoreboardRows = scoreboardJsonConverter.fromJson();
            scoreboardRows.addAll(tempScoreboardRows);
        } catch (FileNotFoundException e) {
            Logger.getLogger(this.getClass()).info("No scoreboard JSON file.");
        }
    }

    // Zapisanie profili do pliku JSON
    public void saveScoreboard() {
        if (scoreboardFile.delete()) {
            Logger.getLogger(this.getClass()).info("Deleted old Scoreboard JSON file.");
        }
        ScoreboardJsonConverter scoreboardJsonConverter = new ScoreboardJsonConverter(scoreboardFile.getPath());
        scoreboardJsonConverter.toJson(scoreboardRows);
    }
}
