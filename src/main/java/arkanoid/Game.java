package arkanoid;

import arkanoid.game_level.game_objects.BasicBlock;
import arkanoid.game_level.game_objects.DoubleBlock;
import arkanoid.game_level.game_objects.SpeedupBlock;
import arkanoid.game_level.handlers.BasicBlockHandler;
import arkanoid.game_level.handlers.DoubleBlockHandler;
import arkanoid.game_level.handlers.SpeedupBlockHandler;
import arkanoid.profile.Profile;
import arkanoid.scoreboard.Scoreboard;
import arkanoid.screens.ChooseDifficulty;
import arkanoid.screens.GameOver;
import arkanoid.screens.LevelComplete;
import arkanoid.screens.MainMenu;
import arkanoid.screens.ProfileMenu;
import arkanoid.screens.State;
import org.apache.log4j.Logger;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Random;

// Glowny watek gry, klasa zarzadza aktualnie wyswietlajacymi sie ekranami
// oraz ich odswiezaniem
public class Game extends Canvas implements Runnable {
    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;

    private State gameState = State.MAIN_MENU;
    private Thread gameThread;
    private boolean running;
    private MainMenu mainMenu;
    private ProfileMenu profileMenu;
    private Profile profile;
    private Scoreboard scoreboard;
    private GameOver gameOver;
    private ChooseDifficulty chooseDifficulty;
    private LevelComplete levelComplete;

    public Game() {
        mainMenu = new MainMenu(this);
        profileMenu = new ProfileMenu(this);
        scoreboard = new Scoreboard(this);
        gameOver = new GameOver(this);
        chooseDifficulty = new ChooseDifficulty(this);
        levelComplete = new LevelComplete(this);
        profile = new Profile();
        levelInit(profile);

        new Window(WIDTH, HEIGHT, "Arkanoid", this);
    }

    // Inicjalizacja poziomu dla danego profilu uzytkownika
    public void levelInit(Profile profile) {
        this.addKeyListener(new PaddleControl(this, profile.getGameLevel().getPaddle()));
        generateBlocks(
                6,
                7,
                profile.getGameLevel().getBasicBlockHandler(),
                profile.getGameLevel().getDoubleBlockHandler(),
                profile.getGameLevel().getSpeedupBlockHandler());
    }

    // Rozpoczecie dzialania watku
    public void start() {
        gameThread = new Thread(this);
        running = true;
        gameThread.start();
    }

    // Zatrzymanie watku, zapisanie profili i tablicy wynikow
    public void stop() {
        try {
            profileMenu.getProfilesManager().saveProfiles();
            scoreboard.getScoreboardManager().saveScoreboard();

            running = false;
            gameThread.join();

            Logger.getLogger(this.getClass()).info("Exiting Arkanoid game.");
            System.exit(0);
        } catch (InterruptedException e) {
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            Logger.getLogger(this.getClass()).warn(stringWriter.toString());
        }
    }

    public void run() {
        // Dzieki tej metodzie nie trzeba klikac na okno,
        // aby okno reagowalo na wcisniete klawisze
        this.requestFocus();

        while (running) {
            tick();
            render();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                StringWriter stringWriter = new StringWriter();
                e.printStackTrace(new PrintWriter(stringWriter));
                Logger.getLogger(this.getClass()).warn(stringWriter.toString());
            }
        }
    }

    public State getGameState() {
        return gameState;
    }

    public void setGameState(State gameState) {
        this.gameState = gameState;
        Logger.getLogger(this.getClass()).info("GameState changed to: " + gameState);
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public ProfileMenu getProfileMenu() {
        return profileMenu;
    }

    // Odswiezanie aktualnego stanu gry
    private void tick() {
        switch (gameState) {
            case GAME:
                if (profile.getGameLevel().tick()) {
                    setGameState(State.GAME_OVER);
                    scoreboard.newScoreboardRow(profile.getGameLevel().getScoreDisplay().getScore(),
                            profile.getName(),
                            profile.getGameLevel().getDifficulty());
                } else if (profile.getGameLevel().isComplete()) {
                    setGameState(State.LEVEL_COMPLETE);
                }
                break;
            case MAIN_MENU:
                mainMenu.tick();
                break;
            case PROFILE_MENU:
                profileMenu.tick();
                break;
            case CHOOSE_PROFILE:
                profileMenu.getChooseProfile().tick();
                profileMenu.getDeleteProfile().tick();
                break;
            case DELETE_PROFILE:
                profileMenu.getDeleteProfile().tick();
                profileMenu.getChooseProfile().tick();
                break;
            case CREATE_PROFILE:
                profileMenu.getCreateProfile().tick();
                break;
        }
    }

    // Rysowanie aktualnego stanu gry
    private void render() {
        BufferStrategy bufferStrategy = this.getBufferStrategy();
        if (bufferStrategy == null) {
            this.createBufferStrategy(3);
            return;
        }
        Graphics graphics = bufferStrategy.getDrawGraphics();

        // Rysowanie tla
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, WIDTH, HEIGHT);

        try {
            switch (gameState) {
                case GAME:
                    profile.getGameLevel().render(graphics);
                    break;
                case MAIN_MENU:
                    mainMenu.render(graphics);
                    break;
                case PROFILE_MENU:
                    profileMenu.render(graphics);
                    break;
                case CHOOSE_PROFILE:
                    profileMenu.getChooseProfile().render(graphics);
                    break;
                case DELETE_PROFILE:
                    profileMenu.getDeleteProfile().render(graphics);
                    break;
                case CREATE_PROFILE:
                    profileMenu.getCreateProfile().render(graphics);
                    break;
                case SCOREBOARD:
                    scoreboard.render(graphics);
                    break;
                case GAME_OVER:
                    profile.getGameLevel().render(graphics);
                    gameOver.render(graphics);
                    break;
                case CHOOSE_DIFFICULTY:
                    chooseDifficulty.render(graphics);
                    break;
                case LEVEL_COMPLETE:
                    profile.getGameLevel().render(graphics);
                    levelComplete.render(graphics);
                    break;
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            Logger.getLogger(this.getClass()).warn(stringWriter.toString());
            System.exit(-1);
        }

        graphics.dispose();
        bufferStrategy.show();
    }

    // Generowanie losowego poziomu
    private void generateBlocks(int rows,
                                int columns,
                                BasicBlockHandler basicBlockHandler,
                                DoubleBlockHandler doubleBlockHandler,
                                SpeedupBlockHandler speedupBlockHandler) {
        int space = 16;
        int x = space;
        int y = space;

        int width = (WIDTH - space - (columns * space)) / (columns);
        int height = (HEIGHT / 2 - space - (rows * space)) / (rows);

        Random random = new Random();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int randomResult = random.nextInt(3);
                if (randomResult == 0) {
                    basicBlockHandler.addGameObject(new BasicBlock(x, y, width, height));
                } else if (randomResult == 1) {
                    doubleBlockHandler.addGameObject(new DoubleBlock(x, y, width, height));
                } else {
                    speedupBlockHandler.addGameObject(new SpeedupBlock(x, y, width, height));
                }
                x += width + space;
            }
            x = space;
            y += height + space;
        }
    }
}
