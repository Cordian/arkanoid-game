package arkanoid.profile;

import arkanoid.game_level.GameLevel;
import org.apache.log4j.Logger;

// Klasa profilu uzytkownika
public class Profile {
    private String name;
    private boolean empty;
    private GameLevel gameLevel;

    public Profile() {
        setDefaultProfile();
    }

    public void newProfile(String name) {
        this.name = name;
        empty = false;
        gameLevel = new GameLevel();
        Logger.getLogger(this.getClass()).info("Created new Profile: " + name);
    }

    public void setDefaultProfile() {
        name = "Guest";
        empty = true;
        gameLevel = new GameLevel();
    }

    public String getName() {
        return name;
    }

    public boolean isEmpty() {
        return empty;
    }

    public GameLevel getGameLevel() {
        return gameLevel;
    }

    public void setGameLevel(GameLevel gameLevel) {
        this.gameLevel = gameLevel;
    }
}
