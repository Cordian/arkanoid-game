package arkanoid.profile;

import arkanoid.Game;
import arkanoid.PaddleControl;
import arkanoid.converters.ProfileJsonConverter;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;
import java.util.Objects;

// Klasa odpowiedzialna za zarzadzanie profilami
public class ProfilesManager {
    private List<Profile> profileList;
    private File directory;
    private Game game;

    public ProfilesManager(List<Profile> profileList, String directoryPath, Game game) {
        this.profileList = profileList;
        directory = new File(directoryPath);
        this.game = game;
    }

    // Wyszukiwanie profili w postaci plikow JSON
    public void findProfiles() {
        if (directory.exists()) {
            Logger.getLogger(this.getClass()).info("Searching profiles.");
            for (File file : Objects.requireNonNull(directory.listFiles())) {
                if (file.exists() && file.isFile()) {
                    if (file.getName().contains("Profile")) {
                        ProfileJsonConverter profileJsonConverter = new ProfileJsonConverter(file.getPath());
                        profileList.add(profileJsonConverter.fromJson());
                    }
                }
            }
        }
    }

    // Przypisanie kazdemu profilowi KeyListenera
    public void loadProfiles() {
        for (Profile profile : profileList) {
            game.addKeyListener(new PaddleControl(game, profile.getGameLevel().getPaddle()));
        }
    }

    // Zapisanie profili do plikow JSON
    public void saveProfiles() {
        if (directory.exists()) {
            // Usuwanie starych plikow JSON
            for (File file : Objects.requireNonNull(directory.listFiles())) {
                System.out.println(file.getPath());
                if (file.delete()) {
                    Logger.getLogger(this.getClass()).info("Deleted old Profile JSON file: " + file.getName());
                }
            }
        }

        Logger.getLogger(this.getClass()).info("Saving profiles.");
        // Zapisanie nowych plikow JSON
        if(!directory.exists()) {
            directory.mkdir();
        }
        for (Profile profile : profileList) {
            if (!profile.isEmpty()) {
                ProfileJsonConverter profileJsonConverter = new ProfileJsonConverter(
                        directory.getPath()
                                + "/"
                                + profile.getName()
                                + "sProfile.json");
                profileJsonConverter.toJson(profile);
            }
        }
    }
}
