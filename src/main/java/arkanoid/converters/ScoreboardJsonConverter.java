package arkanoid.converters;

import arkanoid.scoreboard.ScoreboardRow;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Klasa odpowiedzialna za mapowanie:
 * - plikow JSON na obiekty klasy ScoreboardRow
 * - obiektow klasy ScoreboardRow na pliki JSON
 */
public class ScoreboardJsonConverter {
    private String jsonFilename;
    private Gson gson = new Gson();

    public ScoreboardJsonConverter(String jsonFilename) {
        this.jsonFilename = jsonFilename;
    }

    // ScoreboardRow -> JSON
    public void toJson(List<ScoreboardRow> scoreboardRows) {
        try {
            FileWriter fileWriter = new FileWriter(jsonFilename);
            if (scoreboardRows == null) {
                Logger.getLogger(this.getClass()).warn("ScoreboardRows are NULL, didn't create JSON file.");
                fileWriter.close();
            } else {
                gson.toJson(scoreboardRows, fileWriter);
                fileWriter.close();
                Logger.getLogger(this.getClass()).info("Mapped Scoreboard to JSON file.");
            }
        } catch (IOException e) {
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            Logger.getLogger(this.getClass()).warn(stringWriter.toString());
        }
    }

    // JSON -> ScoreboardRow
    public ArrayList<ScoreboardRow> fromJson() throws FileNotFoundException {
        try {
            FileReader fileReader = new FileReader(jsonFilename);
            ScoreboardRow[] scoreboardRows = gson.fromJson(fileReader, ScoreboardRow[].class);
            fileReader.close();
            Logger.getLogger(this.getClass()).info("Mapped Scoreboard from JSON file.");
            return new ArrayList<>(Arrays.asList(scoreboardRows));
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        } catch (IOException e) {
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            Logger.getLogger(this.getClass()).warn(stringWriter.toString());
        }
        return null;
    }
}
