package arkanoid.converters;

import arkanoid.profile.Profile;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

// Klasa odpowiedzialna za mapowanie:
// - plikow JSON na obiekty klasy Profile
// - obiektow klasy Profile na pliki JSON
public class ProfileJsonConverter {

    private String jsonFilename;
    private Gson gson = new Gson();

    public ProfileJsonConverter(String jsonFilename) {
        this.jsonFilename = jsonFilename;
    }

    // Profile -> JSON
    public void toJson(Profile profile) {
        try {
            FileWriter fileWriter = new FileWriter(jsonFilename);
            if (profile == null) {
                throw new NullPointerException("Profile is null");
            }
            gson.toJson(profile, fileWriter);
            fileWriter.close();
            Logger.getLogger(this.getClass()).info("Mapped Profile: " + profile.getName() + " to JSON file.");
        } catch (NullPointerException | IOException e) {
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            Logger.getLogger(this.getClass()).warn(stringWriter.toString());
        }
    }

    // JSON -> Profile
    public Profile fromJson() {
        try {
            FileReader fileReader = new FileReader(jsonFilename);
            Profile profile = gson.fromJson(fileReader, Profile.class);
            fileReader.close();
            Logger.getLogger(this.getClass()).info("Mapped Profile: " + profile.getName() + " from JSON file.");
            return profile;
        } catch (IOException e) {
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            Logger.getLogger(this.getClass()).warn(stringWriter.toString());
        }
        return null;
    }
}